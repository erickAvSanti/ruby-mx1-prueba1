require 'rspec'
require './functions'

describe Functions do

  it "ejecutando función producto" do
    expect(Functions.product([1, 4, 21])).to eq(84)
    expect(Functions.product([-4, 2.3e12, 77.23, 982, 0b101])).to eq(-3.48863356e+18)
    expect(Functions.product([-3, 11, 2])).to eq(-66)
    expect(Functions.product([8, 300])).to eq(2400)
    expect(Functions.product([234, 121, 23, 945, 0])).to eq(0)
    expect(Functions.product(1..5)).to eq(120)
  end

  it "ejecutando función anagrama" do
    expect(Functions.anagram?('Tap', 'paT')).to be_truthy
    expect(Functions.anagram?('beat', 'table')).to be_falsy
    expect(Functions.anagram?('beat', 'abet')).to be_truthy
    expect(Functions.anagram?('seat', 'teal')).to be_falsy
    expect(Functions.anagram?('god', 'Dog')).to be_truthy
  end

  it "ejecutando función compare" do
    expect(Functions.compare('nice', 'nice')).to be_truthy
    expect(Functions.compare('how', 'who')).to be_falsy
    expect(Functions.compare('GoOd DaY', 'gOOd dAy')).to be_truthy
    expect(Functions.compare('foo', 'food')).to be_falsy
  end

  it "ejecutando función sort_by_key" do
    expect(Functions.sort_by_key(abc: 'hello', another_key: 123, 4567 => 'third')).to eq(["abc", "4567", "another_key"])
  end
  
end
