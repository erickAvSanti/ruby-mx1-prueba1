module Functions
  def self.product(array)
    array.reduce(:*)
  end
  
  def self.anagram?(first, second)
    #first.downcase.split('').reverse.join('') == second.downcase
    hash1 = first.downcase.chars
    .inject({}) do |hash, value|
                  hash[value] = 0 if hash[value].nil?
                  hash[value] += 1 
                  hash
                end
    hash2 = second.downcase.chars
    .inject({}) do |hash, value|
                  hash[value] = 0 if hash[value].nil?
                  hash[value] += 1 
                  hash
                end
    hash1 == hash2
  end
  
  def self.compare(first, second)
    first.casecmp?(second)
  end
  
  def self.sort_by_key(hash)
    hash.sort_by { |key, value| key.to_s.length }.map { |value| value[0].to_s }
  end
end