1. instalar ruby
2. ejecutar irb
3. dentro de irb ejecutar load "functions.rb"
4. ejecutar las funciones de prueba


La diferencia entre super y super() es:

con "super" dentro de un método podemos invocar al método del padre con el
mismo nombre sin necesidad de copiar y pegar 
todos los parámetros para que haga un match, es decir, por defecto se envian los parámetros 
sin que tengamos que especificarlos de manera explícita.

con "super()" dentro de un método podemos invocar al método padre sin especificar 
los argumentos, osea lo invocamos con cero argumentos.